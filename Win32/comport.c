/**************************************************************************************************
 * Filename:       comport.c
 * Description:    This file implements abstraction of windows serial port.
 *                 The module support multiple instances of the serial port.
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */


/**************************************************************************************************
 *                                           Includes
 **************************************************************************************************/

#include <windows.h>

#include "comport.h"
#include <string.h>

/**************************************************************************************************
 *                                           Constant
 **************************************************************************************************/

// Size in bytes of a receive buffer. The size should be determined based on the latency of
// application processing of the buffered content. That is, if the latency is long, the buffer
// size has to be big.
#define COM_BUFFER_SIZE 256

// Number of bytes of data to receive before calling a callback function for the first time.
#define COM_INITIAL_READ_LENGTH 1


/**************************************************************************************************
 *                                        Global Variables
 **************************************************************************************************/


/**************************************************************************************************
 *                                        Local Variables
 **************************************************************************************************/


/**************************************************************************************************
 *                                     Local Function Prototypes
 **************************************************************************************************/
DWORD WINAPI comReadProc(LPVOID lpV);

/**************************************************************************************************
 *
 * @fn          COM_OpenPort
 *
 * @brief       Open a serial port
 *
 * input parameters
 * @param       gszPortName - string identifying name of the port to open
 * @param       cback       - callback function for received data
 * @param       pArg        - an arbitrary argument to be passed to a callback function.
 *
 * output parameters
 * None.
 *
 * @return      COM port device handle.
 */
comDeviceHandle_t COM_OpenPort( wchar_t *gszPortName, comRxCback_t cback, void *pArg )
{
  DWORD dwReadStatId;
  comDeviceHandle_t pDevice;
  DCB dcb = {0, };

  // Build a device data structure
  pDevice = (comDeviceHandle_t) malloc(sizeof(_comDevice_t));
  if (!pDevice)
    return pDevice;

  pDevice->cback = cback;
  pDevice->pArg = pArg;

  // Open serial port
  pDevice->hComm = CreateFile( gszPortName,  
                               GENERIC_READ | GENERIC_WRITE, 
                               0, 
                               0, 
                               OPEN_EXISTING,
                               FILE_FLAG_OVERLAPPED,
                               0);

  // Check if the port was open succesfully
  if (pDevice->hComm == INVALID_HANDLE_VALUE)
  {
    free(pDevice);
    return NULL;
  }

  // Retrieve DCB in order to change the serial port properties.
  dcb.DCBlength = sizeof(dcb);
  if (!GetCommState(pDevice->hComm, &dcb)) {
    // Failed to retrieve DCB. Close device handle and abort.
    CloseHandle(pDevice->hComm);
    free(pDevice);
    return NULL;
  }

  // Build new parameters for the serial port.
  // This particular setting should match the peer device.
//  BuildCommDCB("baud=115200 parity=N data=8 stop=1", &dcb);
  //BuildCommDCB("baud=115200 parity=N data=8 stop=1", &dcb);
  BuildCommDCB("baud=38400 parity=N data=8 stop=1", &dcb);

  if (0)
  {
    // RTS, CTS are used for flow control
    dcb.fOutxCtsFlow = TRUE;
    dcb.fRtsControl = RTS_CONTROL_HANDSHAKE;
  }
  else
  {
    // No flow control
    dcb.fOutxCtsFlow = FALSE;
    dcb.fRtsControl = RTS_CONTROL_DISABLE;
  }

  dcb.EvtChar = '\r';

  // Set the parameters for the device
  if (!SetCommState(pDevice->hComm, &dcb)) {
    // Failed to set the parameters. Abort.
    CloseHandle(pDevice->hComm);
    free(pDevice);
    return NULL;
  }

  // An event is created to communicate a graceful thread termination
  pDevice->hThreadExitEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
  if (pDevice->hThreadExitEvent == NULL) {
    // Failed to create an event. Abort.
    CloseHandle(pDevice->hComm);
    pDevice->hComm = INVALID_HANDLE_VALUE;
    return NULL;
  }

  // Create a serial port reader thread
  pDevice->hReadThread = CreateThread( NULL,
    0,
    (LPTHREAD_START_ROUTINE) comReadProc,
    (LPVOID) pDevice,
    0,
    & dwReadStatId);

  if (pDevice->hReadThread == NULL) {
    // Thread creation failure. Abort.
    CloseHandle(pDevice->hComm);
    CloseHandle(pDevice->hThreadExitEvent);
    free(pDevice);
    return NULL;
  }

  return pDevice;
}

/**************************************************************************************************
 *
 * @fn          COM_ClosePort
 *
 * @brief       Close an open serial port
 *
 * input parameters
 * @param       handle - device handle
 *
 * output parameters
 * None.
 *
 * @return      None.
 */
void COM_ClosePort( comDeviceHandle_t handle )
{
  HANDLE hThreads[1];
  DWORD dwRes;

  // Signal the reader thread to close
  // This mechanism gracefully terminate the thread rather than using abrupt thread termination
  // API.
  hThreads[0] = handle->hReadThread;

  SetEvent(handle->hThreadExitEvent);

  // Wait till the reader thread responds
  dwRes = WaitForMultipleObjects(sizeof(hThreads)/sizeof(HANDLE), hThreads, TRUE, 20000);
  switch (dwRes) {
    case WAIT_OBJECT_0:
      break;
    case WAIT_TIMEOUT:
      // Do nothing
      break;
    default:
      // Do nothing
      break;
  }
  ResetEvent(handle->hThreadExitEvent);

  // Close open windows handles
  CloseHandle(handle->hComm);
  CloseHandle(handle->hReadThread);
  free(handle);
}

/**************************************************************************************************
 *
 * @fn          COM_Write
 *
 * @brief       Write to an open port
 *
 * input parameters
 * @param       handle - device handle returned from COM_OpenPort() function.
 * @param       data   - data to write
 * @param       len    - length in bytes of the data
 *
 * output parameters
 * None.
 *
 * @return      TRUE if the data was written to the serial port buffer successfully.
 *              FALSE, otherwise.
 */
int COM_Write(comDeviceHandle_t handle, const unsigned char *data, unsigned char len)
{
  OVERLAPPED overlapped = {0};
  DWORD lenWritten, result;
  int rv;

  // Build an overlapped structure for asynchronous writing.
  // It has to be done this way as the windows device for the port was open
  // with overlapped attribute, which enables reading and writing asynchronously.
  overlapped.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
  if (overlapped.hEvent == NULL)
    return FALSE;

  // Write
  if (!WriteFile(handle->hComm, data, len, &lenWritten, &overlapped)) {
    // Write did not complete from when WriteFile() returned. This is expected.
    if (GetLastError() != ERROR_IO_PENDING) {
      // Unknown error
      rv = FALSE;
    }
    else {
      // Write is pending, wait for the completion.
      result = WaitForSingleObject(overlapped.hEvent, INFINITE);
      switch (result)
      {
      case WAIT_OBJECT_0:
        // event it was waiting for
        if (!GetOverlappedResult(handle->hComm, &overlapped, &lenWritten, FALSE)) {
          // failure
          rv = FALSE;
        }
        else {
          // success
          rv = TRUE;
        }
        break;
      default:
        // error in waiting for the event
        rv = FALSE;
        break;
      }
    }
  }
  else {
    // error
    rv = FALSE;
  }

  // clean up overlapped structure
  CloseHandle(overlapped.hEvent);

  return rv;
}

/**************************************************************************************************
 *
 * @fn          comReadProc
 *
 * @brief       reader thread entry function
 *
 * input parameters
 * @param       lpV - thread entry function argument, should be set to the handle value of a serial
 *                    port.
 *
 * output parameters
 * None.
 *
 * @return      Non-zero value when the thread exits normally. Zero, otherwise.
 */
DWORD WINAPI comReadProc(LPVOID lpV)
{
  comDeviceHandle_t handle;
  HANDLE hArray[2];
  BOOL fWaitingOnRead = FALSE;
  BOOL fThreadDone = FALSE;
  unsigned char lpBuf[COM_BUFFER_SIZE];
  DWORD dwRead, dwRes, dwNextRead;

  // Build an overlapped structure for read operation
  OVERLAPPED osReader = { 0, };
  handle = (comDeviceHandle_t) lpV;
  osReader.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
  if (osReader.hEvent == NULL) {
    return 0; // error
  }

  // build an array of event handles to use in multi object waiting
  // The thread has to wait for both a read operation completion as well
  // as thread close instruction from the user.
  hArray[0] = osReader.hEvent;
  hArray[1] = handle->hThreadExitEvent;

  // Set the initial read length
  dwNextRead = COM_INITIAL_READ_LENGTH;

  // Loop thru reading unless the user instructed to close the reader thread
  while (!fThreadDone) {

    // New ReadFile() call has to be made unless the asynchronous read operation
    // triggered by previous call hasn't finished.
    if (!fWaitingOnRead) {
      if (!ReadFile(handle->hComm, lpBuf, dwNextRead, &dwRead, &osReader)) {
        if (GetLastError() != ERROR_IO_PENDING) {
          // Error. Return immediately.
          return 0;
        }
        fWaitingOnRead = TRUE;
      }
      else {
        // Read completed immediately, call the callback function.
        if (dwRead)
          dwNextRead = handle->cback(lpBuf, dwRead, handle->pArg);
      }
    }

    // When the asynchronous read operation is still going on from a previous
    // ReadFile() call, wait for the completion.
    if (fWaitingOnRead) {
      dwRes = WaitForMultipleObjects(sizeof(hArray)/sizeof(HANDLE), hArray, FALSE, 1000);
      switch (dwRes) {
        case WAIT_OBJECT_0:
          if (!GetOverlappedResult(handle->hComm, &osReader, &dwRead, FALSE)) {
            if (GetLastError() == ERROR_OPERATION_ABORTED) {
            }
            else {
              return 0;
            }
          }
          else {
            // Read completed successfully
            if (dwRead) {
              // Call the callback function.
              dwNextRead = handle->cback(lpBuf, dwRead, handle->pArg);
            }
          }
          // Now new ReadFile() has to be issued in the next loop.
          fWaitingOnRead = FALSE;
          break;
        case WAIT_OBJECT_0 + 1:
          // User signaled to close the device.
          fThreadDone = TRUE;
          break;
        case WAIT_TIMEOUT:
          break;
        case WAIT_FAILED:
          dwRes = GetLastError();
          // continue to the default handling
        default:
          // It turned out dwRes value is sometimes STATUS_TIMEOUT (0x00000102)
          // Sleep for a second so that other thread can run.
          Sleep(1000);
          break;
      }
    }
  }

  // When this point is reached, the thread is closing.

  CloseHandle(osReader.hEvent);

  return 1;
}
