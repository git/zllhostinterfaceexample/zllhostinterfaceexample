/**************************************************************************************************
 * Filename:       comport.h
 * Description:    This file defines abstract interfaces to serial port.
 *                 The abstraction support only a single instance of a serial port.
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef COMPORT_H
#define COMPORT_H

#include <windows.h>

#ifdef __cplusplus
extern "C" {
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////
// Typedefs

// Callback function pointer type
/**************************************************************************************************
 *
 * @brief       A callback function to be called from comport module upon receipt of data over
 *              the open serial port should use this protytpe.
 *
 * input parameters
 * @param       buf - pointer to a buffer where received data is captured. The application which
 *                    uses the comport module must not dereference this pointer after return of
 *                    this function call. Hence, the application should copy the content of the
 *                    buffer into its own buffer if it intends to process the content later.
 *
 * @param       dwLen - length, in bytes, of received data.
 * @param       pArg - an arbitrary argument which was set to be passed to a callback function
 *                     when the serial interface device is opened.
 *
 * output parameters
 * None.
 *
 * @return      length of the data to read next. Application indicates this length so that the
 *              comport module calls next callback as soon as the indicated number of bytes were
 *              received thru the serial port.
 *              Initial callback will be made when a pre-set number of bytes received. Such is
 *              determined at the compile time of the comport module.
 */
typedef DWORD (*comRxCback_t)( const unsigned char *pBuf, DWORD dwLen, void *pArg );

// Serial port information - data structure may change. Client app should not look inside.
typedef struct {
  HANDLE hComm;             // windows handle to a serial port
  HANDLE hReadThread;       // windows handle of a receive thread
  HANDLE hThreadExitEvent;  // windows handle to an event used to synchronize thread exit
  comRxCback_t cback;       // call back function
  void   *pArg;             // extra argument to pass to a callback function
} _comDevice_t;

// Device Handle
typedef _comDevice_t *comDeviceHandle_t;

/**************************************************************************************************
 *
 * @fn          COM_OpenPort
 *
 * @brief       Open a serial port
 *
 * input parameters
 * @param       gszPortName - string identifying name of the port to open
 * @param       cback       - callback function for received data
 * @param       pArg        - an arbitrary argument to be passed to a callback function.
 *
 * output parameters
 * None.
 *
 * @return      COM port device handle.
 */
  comDeviceHandle_t COM_OpenPort( wchar_t* gszPortName, comRxCback_t cback, void *pArg );

/**************************************************************************************************
 *
 * @fn          COM_ClosePort
 *
 * @brief       Close an open serial port
 *
 * input parameters
 *
 * @param       handle - device handle
 *
 * output parameters
 * None.
 *
 * @return      None.
 */
  void COM_ClosePort( comDeviceHandle_t handle );

/**************************************************************************************************
 *
 * @fn          COM_Write
 *
 * @brief       Write to an open port
 *
 * input parameters
 * @param       handle - device handle returned from COM_OpenPort() function.
 * @param       data   - data to write
 * @param       len    - length in bytes of the data
 *
 * output parameters
 * None.
 *
 * @return      TRUE if the data was written to the serial port buffer successfully.
 *              FALSE, otherwise.
 */
  int COM_Write( comDeviceHandle_t handle, const unsigned char *data, unsigned char len);


#ifdef __cplusplus
}
#endif

#endif COMPORT_H